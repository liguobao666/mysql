链接驱动

链接池
管理工具和服务


innodb短事物

myisam 表锁,空间函数 无事物 小表

B+数有什么缺点：内存碎片，叶分裂

TokuDB分型树的数据结构（写优化）

```
show variables like '%datadir%'

#global 加了global表示全局
set  global datadir ='/db/mysql'

show engines;
```

innodb
	frm文件:描述表结构的文件
	ibd文件:存放表数据 在5.66以后创建独立表空间（逻辑概念），对应文件系统中一个或多个文件 


mysiam
	frm:表结构
	myd:存放数据
	myi:存放索引
	
5.66之前的版本系统表空间（ibdata1 会自动进行扩展）

```
错误日志: show VARIABLES like 'log_error'

慢查询日志

查询日志

二进制文件(bin log)
```

网络通讯：1应用层  2tcp  3ip  4物理层

unix 域套接字（进程间的通信）


# mysql 系统库
```
performance_schema：运行中的信息，（使用情况），既是库又是存储引擎
					events_statements_:记录语句相关的一些数据
					events_stages_:阶段记录
					event_tracsactions_:事物相关

sys：通过试图的方式 组装 performance_schema和information_schema

information_schema：维护其他数据库的一些信息

mysql：统计信息 innodb_index_stats  nnodb_table_stats
		n_diff_pfX01 主键索引
		n_diff_pfx02 
		n_leaf_pages
		
复制信息表:主从复制相关信息
```

查询日志开启
```
set global general_log =on;

set global log_output='Tavle,file'

//查询查询日志
select * from general_log
```

原子：一组操作是原子的

一致性：之前300 之后300

持久性：

隔离性：多个事物之间是隔离的(未提交读，已提交读，可重复读，串行化)

	脏读:读取到了其他事物修改但是未提交的数据

	不可重复度：检索俩次得到不同结果，第一个事物读取到了第二个事物提交后的数据
	
	幻读：第一次读取一条记录，第二次读取到了超过一条（读取到了别人新插入的记录）
	
	
mysql的可重复读隔离级别基本已经解决了幻读的问题
	锁定读  LBCC ( lock base concurrency control)当前读：通过间隙锁
	MVCC 
	
	
## MYsql 事物

 事物基本语法
 
 保存点
 
 隐式提交：1 ddl   2 grant 会把之前的sql默认提交,  3 begin...begin 之间的数据会自动提交 4 Lock table
	

## MYSQl表设计和数据类型优化

1 范试设计
	
	第一范式：
		字段的原子性 name-age--> name  age
		俩列的属性相近或相似,尽量合并属性一样的列,确保不产生冗余数据
		单一属性的列为基本数据类型构成
		设计出来的表都是简单的二维表
	第二范式：
		要求实体的属性完全依赖于主关键字。每一行都有一个唯一性的业务主键
	第三范式：
		一个数据库表中不能存在其他表中非主键的信息
	bcnf范式：
	第四范式：
	第五范式：
	
	
LongAddr 槽位

分库分表进行  数据冗余  用户维度 商品维度

顺序读 随机读  40

顺序写 随机写  10-100

内存与磁盘交互预读页的整数倍（4K 16K）

内存映射mmap

inndb规定16K  


让数据顺序读写